class CreateRationumbers < ActiveRecord::Migration
  def change
    create_table :rationumbers do |t|
      t.text :name
      t.integer :profit
      t.integer :debt

      t.timestamps null: false
    end
  end
end
